import java.util.*

fun main() {
    val read = Scanner(System.`in`)
    while(true) {
        try {
            print("Please input row col : ")
            val row = read.nextInt()
            val col = read.nextInt()
            println("$row, $col")
            break

        } catch (e:InputMismatchException) {
            println("Please input row, col in number format!!!")
            if(read.hasNext()) {
                read.next()
            }
        }
    }
}