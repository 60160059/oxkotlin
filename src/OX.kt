import java.util.*

object OX {
    val kb = Scanner(System.`in`)
    var board = arrayOf(
        charArrayOf(' ', '1', '2', '3'),
        charArrayOf('1', '-', '-', '-'),
        charArrayOf('2', '-', '-', '-'),
        charArrayOf('3', '-', '-', '-')
    )
    var check = 0
    var Break = 0
    var turn = 1
    var r = 0
    var c = 0
    var n = 0
    var player = 'X'

    @JvmStatic
    fun main(args: Array<String>) {
        printWelcome()
        while (true) {
            printBoard()
            input()
            chackWin()
            if (Break == 1) {
                break
            }
            SwitchTurn()
        }
        printBoard()
        printWin()
    }

    fun printWelcome() {
        println("Welcome to OX Game.")
    }

    fun printBoard() {
        for (row in board) {
            for (col in row) {
                print(col + " ")
            }
            println()
        }
    }

    fun input() {
        try {
            while (true) {
                print("$player (R,C): ")
                r = kb.nextInt()
                c = kb.nextInt()

                if(r <= 3 && c <= 3){
                    if (board[r][c] == '-') {
                        board[r][c] = player
                        break
                    } else {
                        println("input mistakes, Please try again.")
                    }
                }
            }
        } catch (e:InputMismatchException){
            println("Please input row, col in number format!!!")
            if(kb.hasNext()) {
                kb.next()
            }
        }
    }

    fun chackWin() {
        if (board[1][1] == board[1][2] && board[1][1] == board[1][3] && board[1][1] != '-' ||
            board[2][1] == board[2][2] && board[2][1] == board[2][3] && board[2][1] != '-' ||
            board[3][1] == board[3][2] && board[1][1] == board[3][3] && board[3][1] != '-'
        ) {
            check = if (player == 'X'){
                1
            }else{
                2
            }
            Break = 1
        }

        if (board[1][1] == board[2][1] && board[1][1] == board[3][1] && board[1][1] != '-' ||
                board[1][2] == board[2][2] && board[1][2] == board[3][2] && board[1][2] != '-' ||
                board[1][3] == board[2][3] && board[1][3] == board[3][3] && board[1][3] != '-'
        ) {
            check = if (player == 'X') {
                1
            } else {
                2
            }
            Break = 1
        }

        if (board[1][1] == board[2][2] && board[1][1] == board[3][3] && board[1][1] != '-' ||
                board[3][1] == board[2][2] && board[1][3] == board[3][1] && board[3][1] != '-'
        ) {
            check = if (player == 'X') {
                1
            } else {
                2
            }
            Break = 1
        }

        Draw()
    }

    fun Draw() {
        if (turn == 9) Break = 1
    }

    fun SwitchTurn() {
        player = if (player == 'X') {
            'O'
        } else {
            'X'
        }
        turn++
    }

    fun printWin() {
        if (check == 1) {
            println("X Winner")
        } else if (check == 2) {
            println("O Winner")
        } else {
            println("Draw")
        }
    }
}

